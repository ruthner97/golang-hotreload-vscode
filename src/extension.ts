import * as vscode from 'vscode';

let restartScheduled: NodeJS.Timeout;

export function activate(context: vscode.ExtensionContext) {
  vscode.workspace.onDidSaveTextDocument(event => {
    clearTimeout(restartScheduled);

    const isGoFile: boolean = event.fileName.endsWith('.go');
    if (isGoFile) {
      restartDebugger();
    }
  });
}

function restartDebugger() {
  restartScheduled = setTimeout(() => {
    if (vscode.debug.activeDebugSession) {
      vscode.commands.executeCommand('workbench.action.debug.restart');
    }
  }, 250);
}

export function deactivate() {
  clearTimeout(restartScheduled);
}
